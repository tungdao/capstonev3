class ExpenseReportController < ApplicationController
  def index
    if request.get?
      @orders = Order.all
    else
      start_date = params[:start_date]
      end_date = params[:end_date]

      unless (start_date.nil? || end_date.nil?)
        @orders = ExpenseReport.buy_in_range_arel(start_date, end_date)
        if start_date > end_date
          respond_to do |format|
            format.html {redirect_to expense_report_url, notice: 'Invalid Input.'}
            format.json {head :no_content}
          end

        end


      end


    end
    # @item_average_price = Order.sum_price
  end
end
