class OrderVendorsController < ApplicationController
  before_action :set_order_vendor, only: [:show, :edit, :update, :destroy]

  # GET /order_vendors
  # GET /order_vendors.json
  def index
    @order_vendors = OrderVendor.all
  end

  # GET /order_vendors/1
  # GET /order_vendors/1.json
  def show
  end

  # GET /order_vendors/new
  def new
    @order_vendor = OrderVendor.new
  end

  # GET /order_vendors/1/edit
  def edit
  end

  # POST /order_vendors
  # POST /order_vendors.json
  def create
    @order_vendor = OrderVendor.new(order_vendor_params)

    respond_to do |format|
      if @order_vendor.save
        format.html { redirect_to @order_vendor, notice: 'Order vendor was successfully created.' }
        format.json { render :show, status: :created, location: @order_vendor }
      else
        format.html { render :new }
        format.json { render json: @order_vendor.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /order_vendors/1
  # PATCH/PUT /order_vendors/1.json
  def update
    respond_to do |format|
      if @order_vendor.update(order_vendor_params)
        format.html { redirect_to @order_vendor, notice: 'Order vendor was successfully updated.' }
        format.json { render :show, status: :ok, location: @order_vendor }
      else
        format.html { render :edit }
        format.json { render json: @order_vendor.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /order_vendors/1
  # DELETE /order_vendors/1.json
  def destroy
    @order_vendor.destroy
    respond_to do |format|
      format.html { redirect_to order_vendors_url, notice: 'Order vendor was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_order_vendor
      @order_vendor = OrderVendor.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def order_vendor_params
      params.require(:order_vendor).permit(:vendor_id, :order_id)
    end
end
