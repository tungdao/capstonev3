class Item < ApplicationRecord
  has_many :order_items
  has_many :orders, through: :order_items

  validates_presence_of :name
  validates :price, numericality: {greater_than: 0}

end
