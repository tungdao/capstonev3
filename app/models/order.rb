class Order < ApplicationRecord
  belongs_to :status
  has_many :order_vendors, dependent: :destroy
  has_many :vendors, through: :order_vendors

  has_many :order_items, dependent: :destroy
  has_many :items, through: :order_items

  validates_presence_of :name

  after_save :calculate_total

  accepts_nested_attributes_for :order_vendors, :allow_destroy => true
  accepts_nested_attributes_for :order_items, :allow_destroy => true

  def calculate_total
    # sub_total = menu_items.map(&:price).sum * menu_orders.collect {|| .}
    sub_total = order_items.collect { |li| li.valid? ? (li.quantity * li.item.price ) : 0 }.sum
    tax = sub_total * 0.10
    update_columns(sub_total: sub_total, tax: tax)
  end

  def total_amount
    sub_total + tax
  end


  def self.sum_price
    Order.sum(:total_amount)
  end

end
