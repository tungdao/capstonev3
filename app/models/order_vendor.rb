class OrderVendor < ApplicationRecord
  belongs_to :vendor
  belongs_to :order
end
