class Vendor < ApplicationRecord
  belongs_to :state
  has_many :order_vendors
  has_many :orders, through: :order_vendors

  validates_presence_of :name
  validates_presence_of :street
  validates_presence_of :zipcode
  validates_presence_of :city
  validates :fname, presence: true
  validates :lname, presence: true
  validates :phone, format: { with: /\d{3}-\d{3}-\d{4}/, message: "bad format" }



end
