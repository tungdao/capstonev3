json.extract! order_vendor, :id, :vendor_id, :order_id, :created_at, :updated_at
json.url order_vendor_url(order_vendor, format: :json)
