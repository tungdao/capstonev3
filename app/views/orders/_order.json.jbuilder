json.extract! order, :id, :name, :date_of_purchase, :sub_total, :tax, :total_amount, :status_id, :created_at, :updated_at
json.url order_url(order, format: :json)
