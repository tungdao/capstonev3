json.extract! vendor, :id, :name, :phone, :fname, :lname, :street, :city, :state_id, :zipcode, :created_at, :updated_at
json.url vendor_url(vendor, format: :json)
