Rails.application.routes.draw do
  get 'location/index'
  get 'expense_report', to: "expense_report#index"
  post 'expense_report', to: "expense_report#index"
  resources :order_items
  resources :items
  resources :order_vendors
  resources :vendors
  resources :states
  resources :orders
  resources :statuses
  get 'menu/index'
  get 'menu/dessert'
  get 'menu/beverages'
  get 'menu/sideitems'
  get 'menu/vermicelli'
  get 'menu/friedriceplate'
  get 'menu/steamedriceplate'
  get 'menu/eggnoodlesoup'
  get 'menu/chickennoodlesoup'
  get 'menu/special'
  get 'menu/beefnoodlesoups'
  get 'menu/appetizers'
  get 'home/index'
  root 'home#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
