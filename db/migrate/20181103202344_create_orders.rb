class CreateOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :orders do |t|
      t.string :name
      t.date :date_of_purchase
      t.decimal :sub_total
      t.decimal :tax
      t.decimal :total_amount
      t.references :status, foreign_key: true

      t.timestamps
    end
  end
end
