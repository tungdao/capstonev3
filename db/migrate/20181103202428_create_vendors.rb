class CreateVendors < ActiveRecord::Migration[5.2]
  def change
    create_table :vendors do |t|
      t.string :name
      t.string :phone
      t.string :fname
      t.string :lname
      t.string :street
      t.string :city
      t.references :state, foreign_key: true
      t.string :zipcode

      t.timestamps
    end
  end
end
