class CreateOrderVendors < ActiveRecord::Migration[5.2]
  def change
    create_table :order_vendors do |t|
      t.references :vendor, foreign_key: true
      t.references :order, foreign_key: true

      t.timestamps
    end
  end
end
