require 'test_helper'

class MenuControllerTest < ActionDispatch::IntegrationTest
  test "should get appetizers" do
    get menu_appetizers_url
    assert_response :success
  end

end
