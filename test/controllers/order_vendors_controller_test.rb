require 'test_helper'

class OrderVendorsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @order_vendor = order_vendors(:one)
  end

  test "should get index" do
    get order_vendors_url
    assert_response :success
  end

  test "should get new" do
    get new_order_vendor_url
    assert_response :success
  end

  test "should create order_vendor" do
    assert_difference('OrderVendor.count') do
      post order_vendors_url, params: { order_vendor: { order_id: @order_vendor.order_id, vendor_id: @order_vendor.vendor_id } }
    end

    assert_redirected_to order_vendor_url(OrderVendor.last)
  end

  test "should show order_vendor" do
    get order_vendor_url(@order_vendor)
    assert_response :success
  end

  test "should get edit" do
    get edit_order_vendor_url(@order_vendor)
    assert_response :success
  end

  test "should update order_vendor" do
    patch order_vendor_url(@order_vendor), params: { order_vendor: { order_id: @order_vendor.order_id, vendor_id: @order_vendor.vendor_id } }
    assert_redirected_to order_vendor_url(@order_vendor)
  end

  test "should destroy order_vendor" do
    assert_difference('OrderVendor.count', -1) do
      delete order_vendor_url(@order_vendor)
    end

    assert_redirected_to order_vendors_url
  end
end
