require "application_system_test_case"

class OrderVendorsTest < ApplicationSystemTestCase
  setup do
    @order_vendor = order_vendors(:one)
  end

  test "visiting the index" do
    visit order_vendors_url
    assert_selector "h1", text: "Order Vendors"
  end

  test "creating a Order vendor" do
    visit order_vendors_url
    click_on "New Order Vendor"

    fill_in "Order", with: @order_vendor.order_id
    fill_in "Vendor", with: @order_vendor.vendor_id
    click_on "Create Order vendor"

    assert_text "Order vendor was successfully created"
    click_on "Back"
  end

  test "updating a Order vendor" do
    visit order_vendors_url
    click_on "Edit", match: :first

    fill_in "Order", with: @order_vendor.order_id
    fill_in "Vendor", with: @order_vendor.vendor_id
    click_on "Update Order vendor"

    assert_text "Order vendor was successfully updated"
    click_on "Back"
  end

  test "destroying a Order vendor" do
    visit order_vendors_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Order vendor was successfully destroyed"
  end
end
