require "application_system_test_case"

class VendorsTest < ApplicationSystemTestCase
  setup do
    @vendor = vendors(:one)
  end

  test "visiting the index" do
    visit vendors_url
    assert_selector "h1", text: "Vendors"
  end

  test "creating a Vendor" do
    visit vendors_url
    click_on "New Vendor"

    fill_in "City", with: @vendor.city
    fill_in "Fname", with: @vendor.fname
    fill_in "Lname", with: @vendor.lname
    fill_in "Name", with: @vendor.name
    fill_in "Phone", with: @vendor.phone
    fill_in "State", with: @vendor.state_id
    fill_in "Street", with: @vendor.street
    fill_in "Zipcode", with: @vendor.zipcode
    click_on "Create Vendor"

    assert_text "Vendor was successfully created"
    click_on "Back"
  end

  test "updating a Vendor" do
    visit vendors_url
    click_on "Edit", match: :first

    fill_in "City", with: @vendor.city
    fill_in "Fname", with: @vendor.fname
    fill_in "Lname", with: @vendor.lname
    fill_in "Name", with: @vendor.name
    fill_in "Phone", with: @vendor.phone
    fill_in "State", with: @vendor.state_id
    fill_in "Street", with: @vendor.street
    fill_in "Zipcode", with: @vendor.zipcode
    click_on "Update Vendor"

    assert_text "Vendor was successfully updated"
    click_on "Back"
  end

  test "destroying a Vendor" do
    visit vendors_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Vendor was successfully destroyed"
  end
end
